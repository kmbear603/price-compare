"use strict"

const express = require("express");
const fs = require("fs");

const app = express();

const PORT = process.env.PORT || 8080;
const PUBLIC = "./build";
const UTF8 = "utf-8";

app.get("/", (req, res)=>{
    res.send(fs.readFileSync(PUBLIC + "/index.html", UTF8));
});

app.get("/*.js", (req, res)=>{
    res.type("text/javascript").send(fs.readFileSync(PUBLIC + req.path, UTF8));
});

app.get("/*.json", (req, res)=>{
    res.type("application/json").send(fs.readFileSync(PUBLIC + req.path, UTF8));
});

app.get("/*.map", (req, res)=>{
    res.send(fs.readFileSync(PUBLIC + req.path));
});

app.get("/*.css", (req, res)=>{
    res.type("css").send(fs.readFileSync(PUBLIC + req.path, UTF8));
});

app.get("*", (req, res)=>{
    res.status(404).send("Not found");
});

app.listen(PORT, ()=>{
    console.log("HTTP server is listening at port " + PORT);
});
