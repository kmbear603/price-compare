"use strict"

import React, { Component } from 'react';
import Paper from "material-ui/Paper";
import TextField from 'material-ui/TextField';

class Offer extends Component {
    constructor(props){
        super(props);
        this.state = {};
    }

    calculatePV(price, volume){
	    const p_val = parseFloat(price);
	    const v_val = parseFloat(volume);
	    return isNaN(p_val) || isNaN(v_val) ? NaN: Math.round(100 * p_val / v_val) / 100;
    }

    onPriceChanged(ev, new_val){
        const pv = this.calculatePV(new_val, this.state.volume);
        this.setState({
            price: new_val,
            pv: pv
        });
        this.props.onPVChanged(this.props.index, pv);
    }
    
    onVolumeChanged(ev, new_val){
        const pv = this.calculatePV(this.state.price, new_val);
        this.setState({
            volume: new_val,
            pv: pv
        });
        this.props.onPVChanged(this.props.index, pv);
    }
    
	render(){
	    const high = this.props.highlight ? " Highlight" : "";
	    
	    const result = isNaN(this.state.pv) ? "" : <TextField readonly fullWidth={ true } underlineStyle={ {
	        borderColor: "#000000",
	        borderStyle: "double",
	        borderWidth: 1
	    } } floatingLabelText="每單位售價" value={ this.state.pv } />;
	    
		return (
		    <div className={ "Offer" + high }>
    			<Paper className="OfferPaper">
    			    <label>貨品 #{ this.props.index }</label><br/>
                    <TextField type="number" fullWidth={ true } hintText="9.9" floatingLabelText="售價" onChange={ this.onPriceChanged.bind(this) } />
                    <TextField type="number" fullWidth={ true } hintText="200" floatingLabelText="內含數量/容量" onChange={ this.onVolumeChanged.bind(this) } />
                    { result }
    			</Paper>
			</div>
		);
	}
}

export default Offer;
