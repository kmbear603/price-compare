"use strict"

import React, { Component } from 'react';
import Offer from "./Offer.js";

class Container extends Component {
	constructor(props){
		super(props);
		this.state = {};
	}
	
	onChanged(index, pv){
		if (index == 1)
			this.setState({ pv1: pv });
		else if (index == 2)
			this.setState({ pv2: pv });
		else
			throw 0;
	}
	
	render(){
		const { pv1, pv2 } = this.state;
		
		var h1, h2;
		if (pv1 && pv2){
			h1 = (pv1 < pv2);
			h2 = (pv2 < pv1);
		}
		else
			h1 = h2 = false;
		
		return (
			<div>
				<div className="Container">
				    <Offer index={ 1 } onPVChanged={ this.onChanged.bind(this) } highlight={ h1 } />
				    <div className="vs">VS</div>
				    <Offer index={ 2 } onPVChanged={ this.onChanged.bind(this) } highlight={ h2 }/>
				</div>
			</div>
		);
	}
}

export default Container;
