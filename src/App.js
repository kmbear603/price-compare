"use strict"

import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Container from './Container.js';

class App extends Component {
	render(){
		return (
			<MuiThemeProvider>
				<div className="App">
					<Container/>
				</div>
			</MuiThemeProvider>
		);
	}
}

export default App;
